# COVID-19 vaccination analysis and projection

Underlying code for manuscript "Spatial analysis of COVID-19 booster vaccine uptake in Scotland, and projection of future distributions"